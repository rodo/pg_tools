# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Push datas in an influxdb database


"""
import os
import argparse
import sys
import logging
from logging.handlers import SysLogHandler
from psycopg2 import connect
from influxdb import InfluxDBClient


__version__ = "0.2.0"
__SCHEMA_NAME__ = "_monyt"


def loglevel(level):
    """Convert string to loglevel values
    """
    return {
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
        'debug': logging.DEBUG}.get(level)


def arg_parse():
    """ Parse command line arguments """
    parser = argparse.ArgumentParser(prog='monyt_influxdb')

    parser.add_argument(
        "-s", "--schema",
        dest="schema_name",
        help="the schema to store results",
        default=__SCHEMA_NAME__)

    parser.add_argument(
        "--host", dest="dbhost",
        help="database host",
        default=None)

    parser.add_argument(
        "-p", "--port", dest="dbport",
        help="database port",
        default=None)

    parser.add_argument(
        "-d", "--db", dest="dbname",
        required=True,
        help="database name",
        default=None)

    parser.add_argument(
        "-U", "--user", dest="dbuser",
        help="user used to connect",
        default=None)

    parser.add_argument(
        "-W", "--password", dest="dbpassword",
        help="password used to connect",
        default=None)

    parser.add_argument(
        "-t", "--timedelta", dest="timedelta",
        help="read datas from database name since",
        default="2 day")

    parser.add_argument(
        "-e", "--environment", dest="environment",
        help="environment tag",
        default=None)

    parser.add_argument(
        "-v", "--verbose", dest="verbose",
        help="verbose mode",
        default=False,
        action="store_true")

    parser.add_argument(
        "--influx-host", dest="influx_host",
        help="influxdb hostname",
        required=True,
        default=None)

    parser.add_argument(
        "--influx-port", dest="influx_port",
        type=int,
        help="influxdb port",
        default=8086)

    parser.add_argument(
        "--influx-user", dest="influx_user",
        help="influxdb username",
        default='root')

    parser.add_argument(
        "--influx-password", dest="influx_password",
        help="influxdb password",
        default='root')

    parser.add_argument(
        "--influx-dbname", dest="influx_dbname",
        help="influxdb dbname",
        default=os.getenv('MONYT_INFLUX_DBNAME'))

    parser.add_argument(
        "--logging-level", dest="logging_level",
        help="logging level",
        choices=['debug','info','warning','error','critical'],
        default='error')

    return parser.parse_args()


def metrics_list():
    """The metrics list

    TODO: read them from the database
    """
    return ['table_size', 'table_seq_scan',
            'table_dead_tuple',
            'table_tuples', 'table_indexes_size']


def dbconnect(dsn, schema_name, verbose):
    """Open a connection to the database
    """
    logging.info("monyt_influxbdb connect to %s" % dsn)
    conn = connect(dsn)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    cur.execute('SET search_path TO %s', (schema_name,))
    return conn


def dbdisconnect(conn):
    """Close the connection from the database server
    """
    conn.close()


def sqlexec(conn, qry, parms=None):
    """Execute a SQL command

    conn : database connection

    qry : SQL command (string)
    """
    cur = conn.cursor()
    cur.execute(qry, parms)
    return cur.fetchall()


def tuples_count(conn, metric, timedelta="2 day"):
    """

    * conn : a postgresql connection

    """
    logging.info('look for %s' % metric)
    tables = []

    # create a db cursor
    cur = conn.cursor()

    parts = ["SELECT",
             "relname,",
             "date_log,",
             "COALESCE(value,0)",
             "FROM metrics",
             "WHERE date_log > now() - interval %s",
             "AND metric=%s",
             "ORDER BY date_log DESC"]

    query = " ".join(parts)

    cur.execute(query, (timedelta, metric))

    for row in cur.fetchall():
        tables.append(
            {
                "table": row[0],
                "time": row[1],
                "value": row[2]
            })

    cur.close()

    return tables


def tuples_delta(conn, timedelta="2 day"):
    """Read data in postgresql database

    * conn : a postgresql connection

    """
    logging.info("monyt_influxbdb look for %s" % timedelta)
    tables = []

    # create a db cursor
    cur = conn.cursor()

    parts = ["SELECT",
             "relname,",
             "fulldate,",
             "COALESCE(count_delta,0)",
             "FROM _monyt.table_delta",
             "WHERE date_log > now() - interval %s",
             "ORDER BY date_log DESC"]

    query = " ".join(parts)

    cur.execute(query, (timedelta,))

    for row in cur.fetchall():
        tables.append(
            {
                "table": row[0],
                "time": row[1],
                "value": row[2]
            }
        )

    cur.close()
    logging.info("monyt_influxbdb found %d tables" % len(tables))
    return tables


def build_json(dbname, environment, key, datas):
    """Build the json to push

    [
        {
        'measurement': 'tuples_count',
        'fields': {'value': 3455.0},
        'tags': {'table': 'leap',
                 'database': 'rodo'},
        'time': datetime.datetime(2017, 1, 1, 19, 6, 34, 178355, tzinfo=psycopg2.tz.FixedOffsetTimezone(offset=60, name=None)),

        }
    ]
    """
    logging.debug("build_json push to %s %s" % (datas['table'], dbname))

    tags = {
        "table": datas['table'],
        "database": dbname
    }

    if environment:
        tags["environment"] = environment

    json_body = [{
        "measurement": key,
        "tags": tags,
        "time": datas['time'],
        "fields": {
            "value": datas['value']
            }
        }]

    return json_body

def influx_client(options):
    """Connect to influx database
    """
    if options.influx_dbname is None:
        logging.critical("""
        Error, The influx db name is not defined, use --influx-dbname option,
        or set MONYT_INFLUX_DBNAME env var""")
        os.sys.exit(2)

    return InfluxDBClient(options.influx_host,
                          options.influx_port,
                          options.influx_user,
                          options.influx_password,
                          options.influx_dbname)


def influx_push(options, key, datas):
    """Push datas on influx API
    """
    json_body = build_json(options.dbname,
                           options.environment,
                           key, datas)

    client = influx_client(options)

    res = client.write_points(json_body, database=options.influx_dbname)

    return res

def push_all(conn, tables, options):
    """Push datas to influxdb server
    """
    for data in tables:
        influx_push(
            options,
            "tuples_delta",
            data
            )
    for metric in metrics_list():
        for data in tuples_count(conn, metric, options.timedelta):
            influx_push(
                options,
                metric,
                data
            )


def main(options):
    # dbname may be specified as option
    dsn = "dbname=%s" % (options.dbname)

    if options.dbport:
        dsn = "%s port=%s" % (dsn, options.dbport)

    if options.dbhost:
        dsn = "%s host=%s" % (dsn, options.dbhost)

    if options.dbuser:
        dsn = "%s user=%s" % (dsn, options.dbuser)

    if options.dbpassword:
        dsn = "%s password=%s" % (dsn, options.dbpassword)

    if options.verbose:
        sys.stdout.write("influxdb host : %s:%s\n" % (options.influx_host,
                                                      options.influx_port))

    conn = dbconnect(dsn, options.schema_name, options.verbose)

    tables = tuples_delta(conn, options.timedelta)

    if options.verbose:
        sys.stdout.write("found %s datas to update %s\n" % (len(tables),
                                                            "tuples_delta"))

    push_all(conn, tables, options)


if __name__ == '__main__':
    options = arg_parse()
    logging.basicConfig(level=loglevel(options.logging_level))

    logger = logging.getLogger()
    logger.addHandler(SysLogHandler('/dev/log'))

    logging.getLogger("requests").setLevel(logging.WARNING)

    logging.info("monyt_influxbdb start")

    if options.influx_host is None:
        logging.critical("monyt_influxbdb influxdb host not defined, exiting")
        sys.exit(1)

    main(options)
    logging.info("monyt_influxbdb stop")
