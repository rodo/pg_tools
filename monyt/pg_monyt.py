# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Compute statistic on databases

You may use a postgres base to log info, in this case you'll need the
file schema.sql

"""
from psycopg2 import connect
import argparse
from psycopg2.extensions import AsIs
import json
import sys
import logging
from logging.handlers import SysLogHandler

__version__ = "1.4.0"
__progname__ = "pg_monyt"
__SCHEMA_NAME__ = "_monyt"


def loglevel(level):
    """Convert string to loglevel values
    """
    return {
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
        'debug': logging.DEBUG}.get(level)

def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-binv]"
    parser = argparse.ArgumentParser(prog=__progname__)

    parser.add_argument(
        "-s", "--schema", dest="schema_name",
        help="the schema to store results",
        default=__SCHEMA_NAME__)

    parser.add_argument(
        "--host", dest="dbhost",
        help="database host",
        default=None)

    parser.add_argument(
        "-p", "--port", dest="dbport",
        help="database port",
        default=None)

    parser.add_argument(
        "-d", "--db", dest="dbname",
        required=True,
        help="database name",
        default=None)

    parser.add_argument(
        "-U", "--user", dest="dbuser",
        help="user used to connect",
        default=None)

    parser.add_argument(
        "-W", "--password", dest="dbpassword",
        help="password used to connect",
        default=None)

    parser.add_argument(
        "-v", "--verbose", dest="verbose",
        help="verbose mode",
        default=False,
        action="store_true")

    parser.add_argument(
        "--logging-level", dest="logging_level",
        help="logging level",
        choices=['debug','info','warning','error','critical'],
        default='error')


    return parser.parse_args()


def dbconnect(dsn, verbose, schema_name):
    """Open a connection to the database
    """
    if verbose:
        sys.stdout.write("Connect to %s" % dsn)
    conn = connect("%s application_name=monyt" % dsn)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    cur.execute('SET search_path TO %s', (schema_name,))
    return conn


def dbdisconnect(conn):
    """Close the connection from the database server
    """
    conn.close()


def dblog(conn, schema_name):
    """Write statistics into database

    - tables : (array) statistics

    """
    cur = conn.cursor()

    iquery = """
    INSERT INTO metrics
    (metric, relnamespace, relname, value)
    (SELECT %s, schemaname,relname, value FROM %s)
    """

    for metric in get_metrics(conn, schema_name):
        res = cur.execute(iquery,
                          (metric[0], AsIs(metric[1])))


    cur.close()

    return res


def log_run(conn, options):
    """Log information on monyt's run

    - conn : psycopg2 connexion

    - options

    """

    query = """
    INSERT INTO log_monyt
    (run_options)
    VALUES
    (%s) RETURNING id
    """

    cur = conn.cursor()

    res = cur.execute(query, ("{}",))
    row = cur.fetchone()
    cur.close()

    return row[0]


def log_stop(conn, run_id, options):
    """Log information on monyt's run

    - conn : psycopg2 connexion

    - options

    """

    query = """
    UPDATE log_monyt
    SET time_stop = now()
    WHERE id = %s
    """

    cur = conn.cursor()

    res = cur.execute(query, (run_id,))
    cur.close()

    return res


def get_metrics(conn, schema_name):
    """Get all metrics name from database
    """
    cur = conn.cursor()

    query = """
    SELECT replace(cl.relname, 'metric_', ''), cl.relname
    FROM pg_class cl
    JOIN pg_namespace na ON cl.relnamespace = na.oid
    WHERE relkind='v'::char
    AND relname like %s
    AND na.nspname = %s
    """

    cur.execute(query, ('metric_%', schema_name))

    metrics = []
    for row in cur.fetchall():
        metrics.append((row[0], row[1]))
    return metrics


def main(options):
    # dbname may be specified as option
    dsn = "dbname=%s" % (options.dbname)

    if options.dbport:
        dsn = "%s port=%s" % (dsn, options.dbport)

    if options.dbhost:
        dsn = "%s host=%s" % (dsn, options.dbhost)

    if options.dbuser:
        dsn = "%s user=%s" % (dsn, options.dbuser)

    if options.dbpassword:
        dsn = "%s password=%s" % (dsn, options.dbpassword)

    conn = dbconnect(dsn, options.verbose, options.schema_name)

    run_id = log_run(conn, vars(options))

    dblog(conn, options.schema_name)

    log_stop(conn, run_id, vars(options))


if __name__ == '__main__':
    options = arg_parse()
    logging.basicConfig(level=loglevel(options.logging_level))
    logger = logging.getLogger()
    logger.addHandler(SysLogHandler('/dev/log'))
    logging.info('%s %s start' % (__progname__, __version__))
    main(options)
    logging.info('%s %s stop' % (__progname__, __version__))
