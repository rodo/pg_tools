# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""

"""
import unittest
from monyt_influxdb import *


class TestInfluxDB(unittest.TestCase):

    def test_loglevel(self):
        self.assertEqual(loglevel('error'), logging.ERROR)
        self.assertEqual(loglevel('info'), logging.INFO)
        self.assertEqual(loglevel('critical'), logging.CRITICAL)
        self.assertEqual(loglevel('warning'), logging.WARNING)
        self.assertEqual(loglevel('debug'), logging.DEBUG)


    def test_build_json(self):
        #
        datas = build_json("database_name",
                           "env",
                           "key",
                           {"table": "table",
                            "time": "time",
                            "value": "value"}
                           )

        self.assertEqual(type(datas), type(list()))
        self.assertEqual(len(datas), 1)
        self.assertEqual(datas[0]['tags']['database'], 'database_name')

    def test_build_json_noenv(self):
        # Without env
        datas = build_json("database_name",
                           None,
                           "key",
                           {"table": "table",
                            "time": "time",
                            "value": "value"}
                           )

        self.assertEqual(type(datas), type(list()))
        self.assertEqual(len(datas), 1)
        self.assertEqual(datas[0]['tags']['database'], 'database_name')


    def test_metrics_list(self):
        # Without env
        datas = metrics_list()

        self.assertTrue(len(datas) > 0)


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    itersuite = unittest.TestLoader().loadTestsFromTestCase(TestInfluxDB)
    runner.run(itersuite)
