--
-- Remember to CREATE SCHEMA admin before
--
SET client_min_messages=WARNING;

SET search_path='_monyt';

--
-- This view need pg_buffercache extension before
--
-- DROP views in ORDER to not use CASCADE option
--
DROP VIEW IF EXISTS table_size;
DROP VIEW IF EXISTS table_delta;
--

CREATE VIEW table_size AS
SELECT c.oid,
    c.relname,
    c.reltuples,
    c.relpages,
    n.nspname

FROM pg_class as c
INNER JOIN
    pg_catalog.pg_namespace n ON n.oid = c.relnamespace
LEFT JOIN
    pg_class p ON c.reltoastrelid = p.oid
WHERE
  c.relkind IN ( 'r' , 't')
AND n.nspname NOT IN ('pg_catalog', 'information_schema')
ORDER BY c.relpages DESC;


--
--
--

CREATE VIEW table_delta AS

SELECT
    date_log::timestamp::date,
    relname, value,
    value-LEAD(value) OVER (PARTITION BY relname ORDER BY date_log DESC) as count_delta,
    date_log - LEAD(date_log) OVER (PARTITION BY relname ORDER BY date_log DESC) as delta_days,
    relnamespace,
    date_log as fulldate

FROM _monyt.metrics
WHERE metric = 'nb_tuples'
ORDER BY relname, date_log desc ;
--
-- Metrics getter views
--
--
-- number of sequential scan per table
--
DROP VIEW IF EXISTS metric_table_seq_scan;
CREATE VIEW metric_table_seq_scan AS
	SELECT
		schemaname,
		relname,
		seq_scan::real as value
	FROM
		pg_catalog.pg_stat_user_tables;

--
-- Dead tuples per table
--
DROP VIEW IF EXISTS metric_table_dead_tup;
CREATE VIEW metric_table_dead_tup AS
	SELECT
		schemaname,
		relname,
		n_dead_tup::real as value
	FROM
		pg_catalog.pg_stat_user_tables;

--
-- Table size
--
DROP VIEW IF EXISTS metric_table_size;
CREATE VIEW metric_table_size AS
	SELECT
		na.nspname as schemaname,
		relname,
		pg_table_size(cl.oid) as value
	FROM
		pg_catalog.pg_class cl
	JOIN
		pg_namespace na ON cl.relnamespace = na.oid
	WHERE relkind='r'::char;

--
-- Indexes size per table
--
DROP VIEW IF EXISTS metric_table_indexes_size;
CREATE VIEW metric_table_indexes_size AS
	SELECT
		na.nspname as schemaname,
		relname,
		pg_indexes_size(cl.oid) as value
	FROM
		pg_catalog.pg_class cl
	JOIN
		pg_namespace na ON cl.relnamespace = na.oid
	WHERE relkind='r'::char;


--
-- Tuples per table
--
CREATE OR REPLACE FUNCTION eval_count(sch text, tablename text)
	RETURNS bigint
	as
	$body$
	declare
	  result bigint;
	begin
	  execute 'SELECT count(*) FROM ' || sch || '.' || tablename INTO result;
	  return result;
	end;
	$body$ LANGUAGE plpgsql;


DROP VIEW IF EXISTS metric_table_tuples;
CREATE VIEW metric_table_tuples AS
	SELECT
		na.nspname as schemaname,
		relname,
		CASE WHEN reltuples::bigint < 10000::bigint
		THEN
		 	(SELECT _monyt.eval_count(na.nspname, relname))
		ELSE
			cl.reltuples::bigint
		END as value
	FROM
		pg_catalog.pg_class cl
	JOIN
		pg_namespace na ON cl.relnamespace = na.oid
	WHERE relkind='r'::char
	AND na.nspname::text NOT IN ('pg_catalog', 'information_schema');
