# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""

"""
import unittest
from monyt_grafana import *


class TestSequenceFunctions(unittest.TestCase):

    def test_grafana_row(self):
        #
        row = grafana_row("title",
                          "table",
                          "database",
                          "src",
                          height="350px",
                          fill=1)

        self.assertEqual(type(row), type({}))

    def test_create_dashboard(self):
        #
        data = {'table': 'foo', 'avg_tuples': 12}
        database = 'monyt'

        dash = create_dashboard(data, database)

        self.assertEqual(type(dash), type({}))

    def test_dashboard_tags(self):
        #
        data = {'table': 'foo', 'avg_tuples': 12}
        database = 'monyt'

        dash = dashboard_tags(data, database, "foo", 1200)

        self.assertEqual(type(dash), type(list()))

    def test_dashboard_tags_small_table(self):
        #
        data = {'table': 'foo', 'avg_tuples': 12}
        database = 'monyt'

        dash = dashboard_tags(data, database, "foo", 1200)

        self.assertEqual(type(dash), type(list()))
        self.assertEqual(len(dash), 2)

    def test_dashboard_tags_big_table(self):
        #
        data = {'table': 'foo', 'avg_tuples': 200}
        database = 'monyt'

        dash = dashboard_tags(data, database, "foo", 100)

        self.assertEqual(len(dash), 3)
        self.assertTrue('bigtable' in dash)


    def test_dashboard_title(self):
        #
        data = {'table': 'foo', 'avg_tuples': 200}

        title = dashboard_title(data, "barfoo", "bar")

        self.assertEqual(title, "bar / barfoo / foo")


    def test_influx_query(self):
        # Test the influx query syntax

        qry = influx_query("barfoo", "bar")

        att = 'SELECT mean(value) FROM "bar" WHERE "table" = \'barfoo\' AND $timeFilter GROUP BY time($interval)'  # noqa

        self.assertEqual(qry, att)


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    itersuite = unittest.TestLoader().loadTestsFromTestCase(TestSequenceFunctions)  # noqa
    runner.run(itersuite)
