# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Create Grafana dashboards


"""
import argparse
import json
import logging
from logging.handlers import SysLogHandler
import requests
import sys
from psycopg2 import connect
from psycopg2.extensions import AsIs
from grafana_api_client import GrafanaClient


__version__ = "0.2.0"
__progname__ = "monyt_grafana"
__SCHEMA_NAME__ = "_monyt"


def loglevel(level):
    """Convert string to loglevel values
    """
    return {
        'info': logging.INFO,
        'warning': logging.WARNING,
        'error': logging.ERROR,
        'critical': logging.CRITICAL,
        'debug': logging.DEBUG}.get(level)

def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-binv]"
    usage = "Usage: %prog " + arg_list
    parser = argparse.ArgumentParser(prog=__progname__)

    parser.add_argument(
        "-s", "--schema", dest="monyt_schema",
        help="the schema to store results",
        default=__SCHEMA_NAME__)

    parser.add_argument(
        "--host", dest="dbhost",
        help="database host",
        default=None)

    parser.add_argument(
        "-p", "--port", dest="dbport",
        help="database port",
        default=None)

    parser.add_argument(
        "-d", "--db", dest="dbname",
        required=True,
        help="database name",
        default=None)

    parser.add_argument(
        "-U", "--user", dest="dbuser",
        help="user used to connect",
        default=None)

    parser.add_argument(
        "-W", "--password", dest="dbpassword",
        help="password used to connect",
        default=None)

    parser.add_argument(
        "-v", "--verbose", dest="verbose",
        help="verbose mode",
        default=False,
        action="store_true")

    parser.add_argument(
        "-o", "--no-overwrite", dest="grafana_overwrite",
        help="overwrite datas",
        default=True,
        action="store_false")

    parser.add_argument(
        "--grafana-host", dest="grafana_host",
        required=True,
        help="grafana hostname",
        default=None)

    parser.add_argument(
        "--grafana-port", dest="grafana_port",
        type=int,
        help="grafana port",
        default=3000)

    parser.add_argument(
        "-k", "--key", dest="grafana_key",
        help="grafana authentification key",
        default=None)

    parser.add_argument(
        "-c", "--datasource", dest="grafana_datasource",
        help="grafana datasource name",
        default="influxdb")

    parser.add_argument(
        "--platform", dest="platform",
        help="platform name monitored",
        default=None)

    parser.add_argument(
        "--logging-level", dest="logging_level",
        help="logging level",
        choices=['debug','info','warning','error','critical'],
        default='error')

    return parser.parse_args()


def dbconnect(dsn, schema_name, verbose):
    """Open a connection to the database
    """
    if verbose:
        sys.stdout.write("Connect to database %s\n" % dsn)
    conn = connect(dsn)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    cur.execute('SET search_path TO %s', (schema_name,))
    return conn


def dbdisconnect(conn):
    """Close the connection from the database server
    """
    conn.close()


def sqlexec(conn, qry, parms=None):
    """Execute a SQL command

    conn : database connection

    qry : SQL command (string)
    """
    cur = conn.cursor()
    cur.execute(qry, parms)
    return cur.fetchall()


def list_tables(conn, options):
    """Return tables names and avg tuples

    * conn : a postgresql connection
    """
    tables = []

    # create a db cursor
    cur = conn.cursor()

    query = """
    SELECT
    relname, avg(value)
    FROM metrics
    WHERE metric='table_tuples'
    GROUP BY relname
    """
    cur.execute(query)

    for row in cur.fetchall():
        tables.append(
            {"table": row[0],
             "avg_tuples": row[1]})

    cur.close()
    return tables


def grafana_headers(key):
    """Return header for HTTP Requests

    auth key
    """
    auth = "Bearer %s" % key
    headers = {"Authorization": auth,
               "Content-Type": "application/json"}

    return headers


def dashboard_tags(datas, database, app, limit):
    """Dashboard tags
    """
    tags = [database, app]

    if datas['avg_tuples'] > limit:
        tags.append('bigtable')

    if datas['avg_tuples'] == 0:
        tags.append('emptytable')

    return tags


def dashboard_title(datas, database, platform=None):
    """Dashboard title
    """
    if platform:
        title = "%s / %s / %s" % (platform,
                                  database,
                                  datas['table'])
    else:
        title = "%s / %s" % (database,
                             datas['table'])
    return  title


def create_dashboard(datas, database, datasource, platform=None, limit=10e4):
    """Create the json for dashboard creation

    """
    table = datas['table']
    app = table.split('_')[0]

    tags = dashboard_tags(datas, database, app, limit)

    title = dashboard_title(datas, database, platform)

    row1 = grafana_row([grafana_panel(datasource,
                                       8,
                                       influx_query(table, "table_tuples"),
                                       table,
                                       "%s count" % table)],
                       "250px")

    row2 = grafana_row([grafana_panel(datasource,
                                      1,
                                      influx_query_difference(table, "table_tuples"),
                                      table,
                                      "%s delta" % table)],
                       "200px")

    row3 = grafana_row([grafana_panel(datasource,
                                       1,
                                       influx_query(table, "table_size"),
                                       table,
                                       "table %s size" % table,
                                       6, 2),
                        grafana_panel(datasource,
                                       6,
                                       influx_query(table, "table_indexes_size"),
                                       table,
                                       "table %s indexes size" % table,
                                       6, 1)
                       ],
                       "220px")


    row4 = grafana_row([grafana_panel(datasource,
                                       1,
                                       influx_query(table, "table_dead_tuple"),
                                       table,
                                       "dead tuples in table %s" % table,
                                       6, 2),
                        grafana_panel(datasource,
                                       6,
                                       influx_query_difference(table, "table_seq_scan"),
                                       table,
                                       "Sequential scans on table %s" % table,
                                       6, 1)
                       ],
                       "220px")


    payload = {
        "id": None,
        "title": title,
        "tags": tags,
        "timezone": "browser",
        "rows": [row1, row2, row3, row4],
        "nav": [
            {
                "collapse": False,
                "enable": True,
                "notice": False,
                "now": True,
                "refresh_intervals": ["1d"],
                "status": "Stable",
                "time_options": [
                    "7d",
                    "30d",
                    "90d",
                    "365d",
                    ],
                "type": "timepicker"
                }
            ],
        "time": {
            "from": "now-120d",
            "now": True,
            "to": "now"
            },
        "schemaVersion": 6,
        "version": 0
        }

    return payload


def influx_query(table, src):
    qry = "SELECT mean(value) FROM \"%s\" WHERE \"table\" = '%s' AND $timeFilter GROUP BY time($interval)" % (src, table)  # noqa
    return qry

def influx_query_difference(table, src):
    qry = "SELECT difference(value) FROM \"%s\" WHERE \"table\" = '%s' AND $timeFilter" % (src, table)  # noqa
    return qry

def grafana_panel(datasource, fill, query, table, title, span=12, id=1):

    return {
            "aliasColors": {},
            "bars": False,
            "datasource": datasource,
            "editable": True,
            "error": False,
            "fill": fill,
            "grid": {
                "leftLogBase": 1,
                "leftMax": None,
                "leftMin": None,
                "rightLogBase": 1,
                "rightMax": None,
                "rightMin": None,
                "threshold1": None,
                "threshold1Color": "rgba(216, 200, 27, 0.27)",
                "threshold2": None,
                "threshold2Color": "rgba(234, 112, 112, 0.22)"
            },
                "id": id,
                "legend": {
                    "avg": False,
                    "current": False,
                    "max": False,
                    "min": False,
                    "show": True,
                    "total": False,
                    "values": False
                    },
                "lines": True,
                "linewidth": 2,
                "links": [],
                "nullPointMode": "connected",
                "percentage": False,
                "pointradius": 5,
                "points": False,
                "renderer": "flot",
                "seriesOverrides": [],
                "span": span,
                "stack": False,
                "steppedLine": False,
                "targets": [
                    {
                        "fields": [
                            {
                                "func": "mean",
                                "name": "value"
                                }
                            ],
                        "groupByTags": [],
                        "measurement": "tuples_count",
                        "query": query,
                        "rawQuery": True,
                        "tags": [
                            {
                                "key": "table",
                                "operator": "=",
                                "value": table
                                }
                            ]
                        }
                    ],
                "timeFrom": None,
                "timeShift": None,
                "title": title,
                "tooltip": {
                    "shared": True,
                    "value_type": "cumulative"
                    },
                "type": "graph",
                "x-axis": True,
                "y-axis": True,
                "y_formats": ["short", "short"]
                }

def grafana_row(panels, height="350px"):

    row = {
        "collapse": False,
        "editable": True,
        "height": height,
        "panels": panels,
        "title": "Row"
        }

    return row


def grafana_call(key, grafana_url, payload):
    """Send a payload to grafana api
    """
    headers = grafana_headers(key)

    r = requests.post(grafana_url,
                      data=json.dumps({"dashboard": payload,
                                       "overwrite": True}), headers=headers)
    return r


def main(options):
    # dbname may be specified as option
    dsn = "dbname=%s" % (options.dbname)

    if options.dbport:
        dsn = "%s port=%s" % (dsn, options.dbport)

    if options.dbhost:
        dsn = "%s host=%s" % (dsn, options.dbhost)

    if options.dbuser:
        dsn = "%s user=%s" % (dsn, options.dbuser)

    if options.dbpassword:
        dsn = "%s password=%s" % (dsn, options.dbpassword)

    conn = dbconnect(dsn, options.monyt_schema, options.verbose)

    grafana_url = 'http://%s:%s/api/dashboards/db' % (options.grafana_host,
                                                      options.grafana_port)

    tables = list_tables(conn, options)

    client = GrafanaClient(options.grafana_key,
                           host=options.grafana_host,
                           port=options.grafana_port)

    if options.verbose:
        sys.stdout.write("Grafana client org %s\n" % client.org())

    for data in tables:
        if options.verbose:
            sys.stdout.write("%s\n" % data)

        payload = create_dashboard(data,
                                   options.dbname,
                                   options.grafana_datasource,
                                   options.platform)

        # grafana_call(options.grafana_key, grafana_url, payload)
        client.dashboards.db.create(dashboard=payload,
                                    overwrite=options.grafana_overwrite)


if __name__ == '__main__':
    options = arg_parse()
    logging.basicConfig(level=loglevel(options.logging_level))
    logger = logging.getLogger()
    logger.addHandler(SysLogHandler('/dev/log'))
    logging.info('%s %s start' % (__progname__, __version__))
    main(options)
    logging.info('%s %s stop' % (__progname__, __version__))
