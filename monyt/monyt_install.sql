--
-- First we create a dedicated schema
--
CREATE SCHEMA IF NOT EXISTS _monyt;
SET search_path TO _monyt;
--
--
CREATE TABLE IF NOT EXISTS catalog_metrics (
	metric_name varchar(25) PRIMARY KEY,
	view_name varchar(25),
	active boolean default true
	);

INSERT INTO catalog_metrics VALUES ('table_seq_scan', 'metric_table_seq_scan');
INSERT INTO catalog_metrics VALUES ('table_dead_tup', 'metric_table_dead_tup');


CREATE TABLE IF NOT EXISTS metrics (
    date_log timestamp with time zone DEFAULT now(),
    relname name,
    relnamespace name,
    metric varchar(20),
    value REAL default -1);

CREATE TABLE IF NOT EXISTS nb_tuples
	(check (metric = 'nb_tuples'))
	INHERITS (metrics);

CREATE INDEX nb_tuples_relname_idx
    ON nb_tuples (relname, date_log desc);

CREATE OR REPLACE FUNCTION on_metrics_insert()
	RETURNS TRIGGER AS $$
	BEGIN
	    IF ( NEW.metric = 'nb_tuples') THEN
	        INSERT INTO _monyt.nb_tuples VALUES (new.*);
		    RETURN NULL;
		ELSE
		    RETURN NEW;
	    END IF;
	END;
	$$ LANGUAGE PLPGSQL;

CREATE TRIGGER metrices_insert
	    BEFORE INSERT on metrics
	    FOR EACH ROW EXECUTE PROCEDURE on_metrics_insert();

--
-- log run's informations
--
CREATE TABLE IF NOT EXISTS log_monyt (
    id serial,
    time_start timestamp WITH TIME ZONE DEFAULT now(),
    time_stop timestamp WITH TIME ZONE,
    run_options json
);

--
--
CREATE TABLE IF NOT EXISTS bi_main (
    date_log timestamp with time zone DEFAULT now(),
    "key" varchar(32) NOT NULL,
    "value" float NOT NULL);

CREATE INDEX bi_main_key_idx
    ON bi_main (key, date_log);
