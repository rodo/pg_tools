How to test
===========

$ createdb pycatalog
$ export PGDATABASE=pycatalog
$ psql < datas/pycatalog.sql
$ py.test tests_pgstat.py

Coverage
========

$ py.test tests_pgstat.py --cov=pgstat.py --cov-report term-missing
    