# -*- coding: utf-8 -*-
#
# Copyright (c) 2015,2016 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""Read stats from pg_stat_statements Postgresql extension

You may use a postgresql base to log info, in this case you'll need the
file schema.sql

"""
import os
import sys
import json
import re
import logging
from psycopg2 import connect, ProgrammingError
from psycopg2.extensions import AsIs
from optparse import OptionParser

__sep__ = '=' * 80

__version__ = "1.7.1"

logger = logging.getLogger(__name__)


def arg_parse():
    """ Parse command line arguments """
    arg_list = "[-binv]"
    usage = "Usage: %prog " + arg_list
    parser = OptionParser(usage, version=__version__)

    parser.add_option(
        "--exclude-user", dest="exclude_users",
        type="string",
        help="username to exclude from check, may be present more than once",
        action="append")

    parser.add_option(
        "--only-user", dest="only_user",
        type="string",
        help="look for query from this username only",
        default=None)

    parser.add_option(
        "-b", "--be-smart", dest="besmart",
        action="store_true",
        help="de smart and do not output status",
        default=False)

    parser.add_option(
        "--logdsn", dest="logdsn",
        type="string",
        help="datasource name to connect to db log",
        default=None)

    parser.add_option(
        "-i", "--ignore-file", dest="ignore_file",
        type="string",
        help="file name to load queries to be ignore",
        default=None)

    parser.add_option(
        "-n", "--name", dest="testname",
        type="string",
        help="the test name to make identification easier",
        default=None)

    parser.add_option(
        "--host", dest="dbhost",
        type="string",
        help="file name to parse",
        default='localhost')

    parser.add_option(
        "-p", "--port", dest="dbport",
        type="string",
        help="file name to parse",
        default='5432')

    parser.add_option(
        "-d", "--db", dest="dbname",
        type="string",
        help="be verbose",
        default=None)

    parser.add_option(
        "-u", "--user", dest="dbuser",
        type="string",
        help="user used too connect to db",
        default="jenkins")

    parser.add_option(
        "-l", "--limit", dest="limit",
        type="int",
        help="detection limit number of rows returned",
        default=100)

    parser.add_option(
        "-r", "--ratio", dest="ratio",
        type="int",
        help="detection limit, correspond to number of same action done",
        default=1)

    parser.add_option(
        "-a", "--actions", dest="nb_action",
        type="int",
        help="detection limit, correspond to number of different actions done",
        default=1)

    parser.add_option(
        "--max-total-calls", dest="max_total_calls",
        type="int",
        help="max of total queries calls",
        default=0)

    parser.add_option(
        "--max-total-rows", dest="max_total_rows",
        type="int",
        help="max of total queries rows",
        default=0)

    parser.add_option(
        "--max-total-time", dest="max_total_time",
        type="int",
        help="max of total queries time",
        default=0)

    parser.add_option(
        "--max-ratio-calls", dest="max_ratio_calls",
        type="int",
        help="max of ratio queries calls",
        default=0)

    parser.add_option(
        "--max-ratio-rows", dest="max_ratio_rows",
        type="int",
        help="max of ratio queries rows",
        default=0)

    parser.add_option(
        "--max-ratio-time", dest="max_ratio_time",
        type="int",
        help="max of ratio queries time",
        default=0)

    parser.add_option(
        "--max-qry-type-warning", dest="max_qry_type_warning",
        type="int",
        help="max query by type, warning limit",
        default=20)

    parser.add_option(
        "--max-qry-type-critical", dest="max_qry_type_critical",
        type="int",
        help="max query by type, critical limit",
        default=50)

    return parser.parse_args()[0]


def dbconnect(dsn):
    """Open a connection to the database
    """
    conn = connect(dsn)
    conn.set_isolation_level(0)
    return conn


def dbdisconnect(conn):
    """Close the connection from the database server
    """
    conn.close()


def sqlexec(conn, qry, parms=None):
    """Execute a SQL command on a connection

    conn : database connection

    qry : SQL command (string)
    """
    cur = conn.cursor()
    cur.execute(qry, parms)
    return cur.fetchall()


def load_ignore(filename):
    """Read a file an returns it's content in json

    Return : json
    """
    try:
        with open(filename) as f:
            datas = f.read()
            content = json.loads(datas)
    except IOError:
        sys.stdout.write('WARNING file %s does not exists\n' % (filename))
        content = []

    return content


def ignore_sign(queries):
    """Return list of sigature from a global dict
    """
    signs = []
    for query in queries:
        signs.append(query['sign'])
    return signs


def too_many_tuples(conn, schema_name, limit, options, igq=None, user=None):
    """Lookup for queries that return too many tuples

    * conn : a postgresql connection

    * schema_name : schema's name where pg_stat_statement is

    * limit : the max number of tuples a query may return (integer)

    * options : dict

    * igq : queries to be ignored (json)

    * user : username to filter on (string)
    """
    datas = []
    queries_signatures = ignore_sign(igq)
    cur = conn.cursor()
    title = 'queries that returned more than %s rows' % (str(limit))
    sys.stdout.write('%s\n%s\n' % (title, '-' * len(title)))

    parts = ["SELECT",
             "u.usename,"
             "d.datname,"
             "s.rows::float / s.calls::float as medrows,",
             "s.rows::int,"
             "s.calls::int,"
             "s.query,"
             "md5(query),",
             "s.total_time,"
             "s.shared_blks_hit,"
             "s.shared_blks_read,"
             "s.shared_blks_written,"
             "s.local_blks_hit,"
             "s.local_blks_read,"
             "s.local_blks_written,"
             "s.temp_blks_read,"
             "s.temp_blks_written",
             "FROM {0}.pg_stat_statements as s",
             "INNER JOIN pg_catalog.pg_user u ON s.userid = u.usesysid",
             "INNER JOIN pg_catalog.pg_database d ON s.dbid = d.oid",
             "WHERE d.datname = %s"]

    parms = (options.dbname,)

    if user is not None:
        parts.append("AND u.usename = %s")
        parms += (user,)

    if options.exclude_users is not None:
        parts.append("AND u.usename NOT IN %s")
        parms += (tuple(options.exclude_users), )

    parts.append("ORDER BY s.rows::float / s.calls::float DESC LIMIT 10")

    query = " ".join(parts)

    cur.execute(query.format(schema_name), parms)

    ignored_queries = 0

    for row in cur.fetchall():
        if row[2] > limit:
            if row[6] in queries_signatures:
                ignored_queries += 1
            else:
                datas.append({"user": row[0],
                              "database": row[1],
                              "median_rows": row[2],
                              "rows": row[3],
                              "calls": row[4],
                              "query": row[5],
                              "query_md5": row[6]})

    if len(queries_signatures):
        msg = '%s ignored queries on %s signatures\n'
        sys.stdout.write(msg % (ignored_queries,
                                len(queries_signatures)))
    return datas


def queries_by_type(conn, schema_name, dbname,
                    ratio,
                    nb_action,
                    single_warn_limit = 20,
                    single_crit_limit = 50,
                    user=None,
                    ignore_users=('jenkins', 'postgres')):
    """
    * conn : a postgresql connection

    * schema_name : schema's name where pg_stat_statement is

    * dbname : database name to filter on (string)

    * ratio : integer

    * nb_action : integer

    # single_warn_limit : upper warning limit by action and user (default 20)

    # single_crit_limit : upper critical limit by action and user (default 50)

    * user : username to filter on (string)

    * ignore_users : ignore theses users

    ================================================================================
    queries by type and user (ratio 10)
    -------------------------------------------------
    select     rodo                     1980    198.0

               rodo                        2      0.2
    SELECT     rodo                        2      0.2
    SET        rodo                        1      0.1
    SHOW       rodo                        1      0.1
    ================================================================================


    """
    datas = []
    cur = conn.cursor()
    title = 'queries by type and user (ratio %s)' % (ratio)
    sys.stdout.write('%s\n%s\n' % (title, '-' * 60))
    ko = 0

    parts = ["SELECT",
             "upper(split_part(query,E' ',1)),",
             "u.usename,"
             "CAST (sum(calls) AS int)",
             "FROM {0}.pg_stat_statements as s",
             "INNER JOIN pg_catalog.pg_database d ON s.dbid = d.oid",
             "INNER JOIN pg_catalog.pg_user u ON s.userid = u.usesysid",
             "WHERE d.datname = %s",
             "AND u.usename NOT IN %s",
             "GROUP BY 1,2 ORDER BY 2,3 DESC"]

    if user is not None:
        parts.append("AND u.usename = %s")

    query = " ".join(parts)

    if user is not None:
        cur.execute(query.format(schema_name), (dbname,
                                                ignore_users,
                                                user))
    else:
        cur.execute(query.format(schema_name), (dbname,
                                                ignore_users,))

    for row in cur.fetchall():
        fmt = "{0:10s} {1:20s} {2:8d} {3:8.1f} {4:8.1f} {5:s}\n"
        rpc = float(row[2]) / float(ratio)
        rpa = rpc / float(nb_action)
        alert = ""

        if rpa > single_crit_limit:
            alert = "critical"
            ko = 1
        elif rpa > single_warn_limit:
            alert = "warning"

        sys.stdout.write(fmt.format(row[0],
                                    row[1],
                                    row[2],
                                    rpc,
                                    rpa,
                                    alert))

    if ko:
        sys.stdout.write('pgstat status : KO\n%s\n' % (__sep__))
    else:
        sys.stdout.write('pgstat status : OK\n%s\n' % (__sep__))


    return datas


def report(conn, schema_name, dbname, nbtable=10):
    """Report the 10 biggest tables

    """
    datas = []
    cur = conn.cursor()

    query = " ".join([
        "SELECT",
        "c.relname,"
        "c.reltuples::int,",
        "n.nspname",
        "FROM pg_class c",
        "INNER JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid",
        "AND n.nspname NOT IN ('pg_catalog')",
        "WHERE c.relkind = 'r'",
        "ORDER BY c.reltuples DESC",
        "LIMIT %s"])

    cur.execute(query, (nbtable, ))

    for row in cur.fetchall():
        datas.append({"name": row[0],
                      "tuples": row[1],
                      "schema": row[2]})

    return datas


def showone(stats, msep, smart):
    result = 0
    fmt = "calls {0:3d}, rows {1:6d}, rpc {3:9.2f} %s\nuser : {4}, sign {5}\n%s\n{2}\n%s" % ('(rpc means rows per call)',  # noqa
                                                                                             msep,
                                                                                             __sep__)
    for stat in stats:
        print fmt.format(stat['calls'],
                         stat['rows'],
                         stat['query'],
                         stat['median_rows'],
                         stat['user'],
                         stat['query_md5'])

    # output or not the check results
    if not smart:
        if len(stats) == 0:
            sys.stdout.write('pgstat status : OK\n%s\n' % (__sep__))
        else:
            sys.stdout.write('pgstat status : KO\n%s\n' % (__sep__))
            result = 1

    return result


def show(stats, nbtable=10):
    sys.stdout.write('%s biggest tables\n%s\n' % (nbtable, '-' * 80))
    fmt = "{0:40s} {1:8d} {2:30s}"
    for stat in stats:
        print fmt.format(stat['name'],
                         stat['tuples'],
                         stat['schema'])

    sys.stdout.write('%s\n' % ('=' * 80))


def nowhere(conn, schema_name, limit, dbname):
    """Lookup for queries without a WHERE clause

    Lookup if there is any queries with a missing WHERE clause

    * conn (psycopg2.connection)

    * schema_name (string)

    * limit (integer)

    * dbname (string)
    """
    title = 'queries without where clause'
    sys.stdout.write('%s\n%s\n' % (title, '-' * len(title)))

    query = """
    SELECT
    u.usename,
    d.datname,
    CAST(s.rows::float / s.calls::float as FLOAT) as medrows,
    s.rows::int,
    s.calls::int,
    s.query,
    md5(query),
    s.total_time
    FROM %s.pg_stat_statements as s
    INNER JOIN pg_catalog.pg_user u ON s.userid = u.usesysid
    INNER JOIN pg_catalog.pg_database d ON s.dbid = d.oid
    WHERE d.datname = %s
    AND rows / calls > 100.0
    AND query LIKE 'SELECT %%'
    AND NOT (query LIKE '%%WHERE%%')
    """

    result = sqlexec(conn, query, (AsIs(schema_name), dbname))

    datas = []
    for row in result:
        datas.append({"user": row[0],
                      "database": row[1],
                      "median_rows": row[2],
                      "rows": row[3],
                      "calls": row[4],
                      "query": row[5],
                      "query_md5": row[6]})
    return datas


def column_not_indexed(conn, schema_name, limit, dbname, ratio=1):
    """Lookup for queries without a WHERE clause

    Lookup if there is any queries with a missing WHERE clause

    * conn (psycopg2.connection)

    * schema_name (string)

    * limit (integer)

    * dbname (string)
    """
    title = 'column may be indexed'
    sys.stdout.write('%s\n%s\n' % (title, '-' * len(title)))

    query = """
        SELECT
        s.query,
        s.calls::int,
        s.rows
        FROM %s.pg_stat_statements AS s
        INNER JOIN pg_catalog.pg_database d ON s.dbid = d.oid
        WHERE d.datname = %s
        AND query ILIKE '%%WHERE%%'
        AND NOT query LIKE '%%pg_catalog%%'
        ORDER BY s.calls DESC
        """

    result = sqlexec(conn,
                     query,
                     (AsIs(schema_name), dbname)
                     )

    datas = field_frequency(result)
    columns = split_table(datas)

    for col in columns:
        ici = is_column_indexed(conn, schema_name, dbname,
                                col['table'],
                                col['column'])


        score = index_score(col['value'], ratio)

        if not ici:
            sys.stdout.write('score %3d for %s.%s\n' % (
                score,
                col['table'],
                col['column']))

    sys.stdout.write("%s\n" % ('=' * 80))

    return datas


def index_score(raw_value, ratio=1):
    """Compute index not used score


    raw_value (integer)
    """
    if ratio == 0:
        ratio = 1

    score = raw_value / ratio

    return score


def is_column_indexed(conn, schema_name, dbname, table, column):
    """Is a column indexed
    """
    query = """
    WITH col AS (
    SELECT DISTINCT c.relname, c.oid, unnest(indkey) as colid
    FROM pg_catalog.pg_index i, pg_catalog.pg_class c
    WHERE c.relname=%s AND i.indrelid=c.oid
    )

    SELECT a.attname, a.attnum
    FROM pg_catalog.pg_attribute a, col

    WHERE a.attrelid = col.oid
    AND col.colid = a.attnum
    AND a.attname=%s
    """

    result = sqlexec(conn, query, (table, column))

    return len(result)


def split_table(datas):
    """

    {'"fish_fish"."title_size"': 5,
     '"fish_fish"."body_size"': 1,
     '"bar_client"."id"': 1000}


     Return : [{'column': 'title_size',
                'table': 'fish_fish',
                'value': 1}]

    """
    columns = []

    for key in datas:
        (table, column) = tuple([a[1:-1] for a in key.split('.')])
        columns.append({"table": table,
                        "column": column,
                        "value": datas[key]})
    return columns


def field_frequency(result):
    """

    Return : {'table.title_size': 2,
              'table.body_size': 4}
    """
    reg = r'("?\w+"?\."?\w+"?)\s+=\s+'
    search = re.compile(reg, re.DOTALL)

    datas = {}
    for row in result:

        qry = row[0]
        vals = search.findall(qry)
        if vals:
            for val in vals:
                try:
                    datas[val] = datas[val] + row[1]
                except:
                    datas[val] = row[1]
    return datas


def twentyone(conn, schema_name, dbname):
    """Lookup for queries with LIMIT 21 clause

    * conn (psycopg2.connection)

    * schema_name (string)

    * dbname (string)
    """
    title = 'queries with LIMIT 21 clause'
    sys.stdout.write('%s\n%s\n' % (title, '-' * len(title)))

    query = """
    SELECT
    u.usename, d.datname,
    CAST(s.rows::float / s.calls::float as FLOAT) as medrows,
    s.rows::int, s.calls::int, s.query,
    md5(query)
    FROM {0}.pg_stat_statements as s
    INNER JOIN pg_catalog.pg_user u ON s.userid = u.usesysid
    INNER JOIN pg_catalog.pg_database d ON s.dbid = d.oid
    WHERE d.datname = %s
    AND (s.rows / s.calls) = 21
    AND query LIKE %s
    """

    result = sqlexec(conn,
                     query.format(schema_name),
                     (dbname, '% LIMIT%'))

    msep = '-' * 3
    sep = '-' * 80
    fmt = "calls {0:3d}, rows {1:6d}, rpc {3:9.2f} %s\nuser : {4}, sign {5}\n%s\n{2}\n%s\n" % ('(rpc means rows per call)',  # noqa
                                                                                   msep,
                                                                                   sep)
    datas = []
    for row in result:
        sys.stdout.write(fmt.format(row[4],
                                    row[3],
                                    row[5],
                                    row[2],
                                    row[1],
                                    row[6]
                                    ))

    sys.stdout.write("%s\n" % ('=' * 80))

    return datas


def statsum(conn, schema_name, dbname):
    """Read statistics on queries

    schema_name

    dbname

    Return : (dict)
    """
    cur = conn.cursor()

    query = " ".join(["SELECT",
                      "CAST(sum(s.rows) AS INT), CAST(sum(s.calls) AS INT),",
                      "sum(total_time)",
                      "FROM {0}.pg_stat_statements as s",
                      "INNER JOIN pg_catalog.pg_database d ON s.dbid = d.oid",
                      "WHERE d.datname = %s"])

    cur.execute(query.format(schema_name), (dbname, ))

    row = cur.fetchone()
    datas = {"rows": row[0],
             "calls": row[1],
             "total_time": row[2]}
    return datas


def strStatus(status):
    if status:
        return 0, 'OK'
    else:
        return 1, 'KO'


def linesum(label, value, limit):
    status, msg = sublinesum(label, value, limit)
    sys.stdout.write(msg)
    return status


def sublinesum(label, value, limit):
    status = 0
    if limit > 0:
        status, str_status = strStatus(value < limit)
        msg = 'pgstat %-12s : %6d, limit %6d %11s\n' % (label,
                                                        value,
                                                        limit,
                                                        str_status)
    else:
        # the limit is not checked
        msg = 'pgstat %-12s : %6d\n' % (label, value)
    return status, msg


def showsum(stats, options, prev=None):
    """Print information on standard output
    """
    status = 0
    sys.stdout.write('main stats, ratio=%s\n%s\n' % (options.ratio, '-' * 60))

    status += linesum('rows total', stats['rows'], options.max_total_rows)
    status += linesum('rows / qry', float(stats['rows']) / float(options.ratio), options.max_ratio_rows)

    status += linesum('calls total', stats['calls'], options.max_total_calls)
    status += linesum('calls / qry', float(stats['calls']) / float(options.ratio), options.max_ratio_calls)

    status += linesum('time total', stats['total_time'], options.max_total_time)
    status += linesum('time / qry', float(stats['total_time']) / float(options.ratio), options.max_ratio_time)

    if prev:
        print_delta('rows', stats['rows'], prev['rows'])
        print_delta('calls', stats['calls'], prev['calls'])
        print_delta('time', stats['total_time'], prev['total_time'])

    final_status = 'OK'
    if status > 0:
        final_status = 'KO'

    if not options.besmart:
        sys.stdout.write('pgstat status : %s\n' % (final_status))

    sys.stdout.write('%s\n' % ('-' * 60))

    return int(status > 0)


def print_delta(key, current, previous):
    msg = compute_delta(key, current, previous)
    sys.stdout.write(msg)


def compute_delta(key, current, previous):
    """Format string delta
    """
    delta = float(current - previous) / previous * 100.0

    msg = 'pgstat %-5s delta  : %6d\npgstat %-5s deltap : %6.2f%%\n' % (key,
                                                                        current - previous,
                                                                        key,
                                                                        delta,
                                                                        )

    return msg


def dblog(stats, options):
    """Write statistics into database

    - stats : (dict) statistics
    - options (dict) command line options

    """
    conn = dbconnect(options.logdsn)

    query = """
    INSERT INTO gatling_log
    (testdate, testname
    , returned_rows, total_calls, total_time
    , ratio, options)
    VALUES
    (now(), %s, %s, %s, %s, %s, %s)
    """

    cur = conn.cursor()

    jsopt = {'ratio': options.ratio,
             'nb_action': options.nb_action,
             'max_qry_type_critical': options.max_qry_type_critical}

    res = cur.execute(query, (options.testname,
                              stats['rows'],
                              stats['calls'],
                              stats['total_time'],
                              int(options.ratio),
                              json.dumps(jsopt)))

    conn.close()

    return res


def previous_run(options):
    """Read statistics of the previous run

    - options (dict) command line options

    """
    conn = dbconnect(options.logdsn)

    query = """
    SELECT returned_rows, total_calls, total_time
    FROM gatling_log
    WHERE testname = %s
    ORDER BY testdate DESC
    LIMIT 1
    """

    cur = conn.cursor()
    cur.execute(query, (options.testname,))
    row = cur.fetchone()

    try:
        stats = {"rows": row[0],
                 "calls": row[1],
                 "total_time": row[2]}
    except TypeError:
        stats = None

    conn.close()

    return stats


def usersid(options):
    """Read the userid from pg_user table

    - users (list) the user's name we are looking for

    """
    conn = dbconnect(options.logdsn)

    query = """
    SELECT array_to_string(array_agg(usesysid),',')
    FROM pg_catalog.pg_user
    WHERE usename IN (%s)
    """

    cur = conn.cursor()
    cur.execute(query, (",".join(options.exclude_users)))
    row = cur.fetchone()
    res = row[0]
    if len(res) == 0:
        res = None

    return res


def header(f, options):
    """Print header

    f : file descriptor

    options : OptParse object
    """
    f.write('%s\npgstat start, version %s\n' % (__sep__, __version__))
    f.write('pgstat limit   : %s\n' % (options.limit))
    f.write('pgstat ratio   : %s\n' % (options.ratio))
    if options.nb_action > 1:
        f.write('pgstat actions : %s\n' % (options.nb_action))

    if options.exclude_users is not None:
        f.write('pgstat exclude user(s) : %s\n' % (",".join(options.exclude_users)))

    if only_user is not None:
        f.write('pgstat only user : %s\n' % (only_user))


if __name__ == '__main__':
    result = 0
    i = 0

    msep = '-' * 38
    prev = None
    ignore_queries = []

    options = arg_parse()
    only_user = os.environ.get('PGSTAT_ONLY_USER', options.only_user)

    header(sys.stdout, options)

    sys.stdout.write('%s\n' % (__sep__))

    # will be optionnal later
    schema_name = "dba"

    if options.ignore_file:
        ignore_queries = load_ignore(options.ignore_file)

    # the password is set in .pgpass in user's homedir
    dsn = "host=%s port=%s dbname=%s user=%s" % (options.dbhost,
                                                 options.dbport,
                                                 options.dbname,
                                                 options.dbuser)

    conn = dbconnect(dsn)

    stats = too_many_tuples(conn, schema_name, options.limit, options,
                            ignore_queries,
                            only_user)

    result = result + showone(stats, msep, options.besmart)

    stats = nowhere(conn, schema_name, options.limit, options.dbname)
    result = result + showone(stats, msep, options.besmart)

    stats = report(conn, schema_name, options.dbname)
    show(stats)

    # queries with LIMIT 21
    twentyone(conn, schema_name, options.dbname)

    stats = queries_by_type(conn, schema_name,
                            options.dbname,
                            options.ratio,
                            options.nb_action,
                            options.max_qry_type_warning,
                            options.max_qry_type_critical)

    stats = statsum(conn, schema_name, options.dbname)

    # compare with last run
    if options.testname and options.logdsn:
        prev = previous_run(options)
        dblog(stats, options)

    result = result + showsum(stats, options, prev)

    stats = column_not_indexed(conn,
                               schema_name,
                               options.limit,
                               options.dbname,
                               options.ratio)

    # end
    sys.stdout.write('pgstat stop\n')
    dbdisconnect(conn)
    # need to exit with result code to mark jenkin's job as failed
    if options.besmart:
        sys.exit(0)
    else:
        sys.exit(result)
