# -*- coding: utf-8 -*-
#
# Copyright (c) 2015 Rodolphe Quiédeville <rodolphe@quiedeville.org>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""

"""
import unittest
from pgstat import *


class TestSequenceFunctions(unittest.TestCase):

    def test_index_score(self):
        #
        row = index_score(20, 4)
        self.assertEqual(row, 5)

    def test_index_score_zero(self):
        #
        row = index_score(20, 0)
        self.assertEqual(row, 20)

    def test_field_frequency(self):
        #
        datas = [('select * from fish_fish where body_size = ?;', 3),
                 ('SELECT * FROM "public"."fish_fish" WHERE "fish_fish"."title_size" = ?;', 1)]
                
        row = field_frequency(datas)
        self.assertEqual(row, {'"fish_fish"."title_size"': 1})


    def test_field_frequency_two(self):
        #
        datas = [('SELECT (1) FROM fish_fish WHERE "fish_fish"."title_size" = ?;', 3),
                 ('SELECT * FROM "public"."fish_fish" WHERE "fish_fish"."title_size" = ?;', 1)]
                
        row = field_frequency(datas)
        self.assertEqual(row, {'"fish_fish"."title_size"': 4})


    def test_field_frequency_regular(self):
        #
        datas = [('SELECT (1) FROM fish_fish WHERE "title_size" = ?;', 1),
                 ('SELECT (1) FROM fish_fish WHERE public.title_size = ?;', 2),
                 ('SELECT (1) FROM fish_fish WHERE title_size = ?;', 3)]
                
        row = field_frequency(datas)
        self.assertEqual(row, {'public.title_size': 2})


    def test_split_table(self):
        #
        datas = {'"fish_fish"."title_size"': 1}
                
        row = split_table(datas)
        self.assertEqual(row, [{'column': 'title_size',
                                'table': 'fish_fish',
                                'value': 1}])

    def test_split_empty(self):
        # empty datas
        datas = {}
                
        row = split_table(datas)
        self.assertEqual(row, [])



if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    itersuite = unittest.TestLoader().loadTestsFromTestCase(TestSequenceFunctions)  # noqa
    runner.run(itersuite)
