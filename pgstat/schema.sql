--
--
--
CREATE TABLE gatling_log (
        testname character varying,
        testdate timestamp with time zone DEFAULT now(),
        returned_rows integer,
        total_calls integer,
        total_time double precision,
        ratio integer,
        options jsonb
        );
